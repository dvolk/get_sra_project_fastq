# Description

Downloads paired fastq files from ncbi SRA projects

# Usage

./get_sra_project_fastq.sh [PROJECT_ID]

# Requirements

- sra-tools: https://github.com/ncbi/sra-tools/wiki/HowTo:-Binary-Installation
  - download http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/sratoolkit.current-ubuntu64.tar.gz
  - extract
  - add sratoolkit.2.8.2-1-ubuntu64/bin to PATH
- python-q-text-as-data
  - apt install python-q-text-as-data
- wget
  - apt install wget

# Known issues

- fastq-dump uses a cache directory which is by default in ~/ncbi, if you have insufficient space to hold the cache there you should move it elsewhere with vdb-config -i, or disable it (not recommended)
