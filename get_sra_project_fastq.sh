# Requirements:
#
# apt install sra-tools python-q-text-as-data wget
#

# exit if a command returns non-zero
set -e

PROJECT="$1"
PROJECT_DIR="sra_${PROJECT}"
PROJECT_CSV_FILE="${PROJECT_DIR}/${PROJECT}.csv"

if [[ ! ${PROJECT} ]]; then
    echo "Use ./${0} <PROJECT_ID>"
    exit 1
fi

# Create project fastq directory
mkdir -p "${PROJECT_DIR}"

# Download csv containing sample ids from ncbi
if [[ ! -e "${PROJECT_CSV_FILE}" ]]; then
    echo -n "Downloading project csv file ... "
    wget -q -O "${PROJECT_CSV_FILE}" "http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term=${PROJECT}"
    echo "OK"
else
    echo "Project csv file already exists"
fi

# Remove empty lines from csv
sed -i '/^$/d' "${PROJECT_CSV_FILE}"

# Get total sample count
SAMPLE_COUNT=$(q -H -d',' "select count(*) from ${PROJECT_CSV_FILE}")

if [[ ${SAMPLE_COUNT} -eq 0 ]]; then
    echo "Error: Downloaded project CSV but no samples found"
    exit 1
fi

# Download fastq files from ncbi
SAMPLE_N=1
RUNS=$(q -H -d',' "select Run from ${PROJECT_CSV_FILE}")
LOG_DIR="${PROJECT_DIR}/logs"
mkdir -p "${LOG_DIR}"
for RUN in ${RUNS}; do
    TIMENOW=$(date)
    RUN="${RUN}"
    echo -n "${TIMENOW} Downloading ${RUN} [${SAMPLE_N}/${SAMPLE_COUNT}] ... "
    LOGFILE="${LOG_DIR}/${RUN}.fastq-dump.log"
    echo $(date) >> "${LOGFILE}"
    echo $(date) >> "${LOG_DIR}/${PROJECT}.log"
    echo ${RUN} >> "${LOG_DIR}/${PROJECT}.log"
    RET=$(fastq-dump --gzip --split-3 "${RUN}" -O "${PROJECT_DIR}/" > "${LOGFILE}" 2>&1)
    cat "${LOGFILE}" >> "${LOG_DIR}/${PROJECT}.log"
    if [[ ${RET} -eq 0 ]]; then
        echo "OK"
    else
        echo "Failed"
        exit 1
    fi
    SAMPLE_N=`expr ${SAMPLE_N} + 1`
done
